This is a simple blog made as a part of learning experience.

This is made using : 
	1. React for frontend (with Redux), 
	2. Node for backend (Express.JS - node framework),
	3. MongoDB for database.
	4. create-react-app (Obviously)
	
Use : `mongod`  to start MongoDB server in another terminal.

`nodeidon` daemon is started by `npm run dev`.

## App ScreenShots

### **Feed**
![](screenShots/feed.png)


### **SignInWith Google**
![](screenShots/signinwith.png)
