import { combineReducers } from 'redux';
import articles from './reducers/articles.js';
import authUser from './reducers/authUser.js';
import common from './reducers/common.js';
import { routerReducer } from 'react-router-redux';
export default combineReducers({
  articles,
  authUser,
  common,
  router: routerReducer
});